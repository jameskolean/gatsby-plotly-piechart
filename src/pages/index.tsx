// /sec/pages/index.tsx
import * as React from 'react'
import loadable from '@loadable/component'

const IndexPage = () => {
  const Plot = loadable(() => import('react-plotly.js'))
  const [data, setData] = React.useState<MyPlotData[]>([
    {
      values: [19, 26, 55],
      labels: ['Residential', 'Non-Residential', 'Utility'],
      pull: [0, 0, 0],
      type: 'pie'
    }
  ])

  const layout = {
    height: 400,
    width: 500
  }

  function handleClickOnPie(e: Plotly.PlotMouseEvent) {
    const newData = [...data]
    const sliceIndex = e.points[0].pointNumber
    if (newData[0].pull[sliceIndex] > 0) {
      newData[0].pull[sliceIndex] = 0
    } else {
      newData[0].pull[sliceIndex] = 0.15
    }
    setData(newData)
  }

  function handleClickOffPie() {
    const newData = [...data]
    newData[0].pull = [0, 0, 0]
    setData(newData)
  }

  return (
    <main>
      <title>Home Page</title>
      <h1> My First Plotly Chart</h1>
      <div onClick={handleClickOffPie}>
        <Plot data={data} layout={layout} onClick={handleClickOnPie} />
      </div>
    </main>
  )
}

export default IndexPage

interface MyPlotData extends Partial<Plotly.PlotData> {
  pull: number[]
}
